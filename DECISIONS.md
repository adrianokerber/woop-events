# Decisions

### 2018.09.16
* Created the project and decided to create an entire SDK for API access inside the codebase
* Decided to use Realm DB for data control inside app
* Decided to add SwipeRefresh to improve UX with an easy refresh machanism
* Decided to add a hover button for Checkin that whould opens an AlertDialog for checkin