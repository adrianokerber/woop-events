# WoopEvents App
Developed by [Adriano Kerber](mailto:kerberpro@gmail.com)

This app was developed to display Woop's events, its details and allow user checkin to events.

## Requirements

* Android Studio (v3.0.0 or above)

## Setup

Just clone this repository and open the project folder with the Android Studio IDE.

## Usage

### This app covers

* Unit test for all SDK methods of EventAPI
* Swipe down for refresh main screen
* Click on event opens the details screen
* Button for checkin in the bottom end of details screen
* Images load for events and users

### Personal notes

* The API service should use token authentication for any edit calls like POST, DELETE, etc
* The API service should have options for requesting images with different resolutions, to avoid long downloads unnecessarely. Some examples:
| portrait_small | 50x75px
| portrait_medium | 100x150px
| portrait_xlarge | 150x225px
| portrait_fantastic | 168x252px
| portrait_uncanny | 300x450px
| portrait_incredible | 216x324px

## Third Party libraries

* [Retrofit](https://square.github.io/retrofit/)
* [Picasso](http://square.github.io/picasso/)
* [Realm DB](https://realm.io/products/realm-database/)
* [ReactiveX](http://reactivex.io/)