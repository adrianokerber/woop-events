package com.adrianokerber.woopevents;

import android.support.test.runner.AndroidJUnit4;

import com.adrianokerber.woopevents.framework.util.RetryWithDelay;
import com.adrianokerber.woopevents.framework.sdk.EventSDK;
import com.adrianokerber.woopevents.framework.sdk.event.EventAPI;
import com.adrianokerber.woopevents.framework.sdk.event.model.Checkin;

import org.junit.Test;
import org.junit.runner.RunWith;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static org.junit.Assert.*;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
@RunWith(AndroidJUnit4.class)
public class EventSDKTest {
    private Integer timeout = 90000;

    public EventSDKTest() {
        // Initialize SDK
        EventSDK.get().initialize();
    }

    @Test
    public void getList() throws Exception {
        Object syncObject = new Object();

        // Given
        // ...

        // When
        EventAPI.get().getList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retryWhen(new RetryWithDelay(3, 2000))
                .subscribe(result -> {
                    // Then
                    assertEquals(true, result != null);
                    synchronized (syncObject) {
                        syncObject.notify();
                    }
                }, throwable -> {
                    // Then
                    assertEquals(true, false);
                    synchronized (syncObject) {
                        syncObject.notify();
                    }
                });

        synchronized (syncObject) {
            syncObject.wait(timeout);
        }
    }

    @Test
    public void getEvent() throws Exception {
        Object syncObject = new Object();

        // Given
        String eventId = "1";

        // When
        EventAPI.get().getEvent(eventId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retryWhen(new RetryWithDelay(3, 2000))
                .subscribe(result -> {
                    // Then
                    assertEquals(true, result != null);
                    synchronized (syncObject) {
                        syncObject.notify();
                    }
                }, throwable -> {
                    // Then
                    assertEquals(true, false);
                    synchronized (syncObject) {
                        syncObject.notify();
                    }
                });

        synchronized (syncObject) {
            syncObject.wait(timeout);
        }
    }

    @Test
    public void postCheckin() throws Exception {
        Object syncObject = new Object();

        // Given
        String eventId = "1";
        String name = "Hélio dos Passos";
        String email = "heliodaspassos@show.com";

        // When
        EventAPI.get().postCheckin(new Checkin(eventId, name, email))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retryWhen(new RetryWithDelay(3, 2000))
                .subscribe(result -> {
                    // Then
                    assertEquals(true, true);
                    synchronized (syncObject) {
                        syncObject.notify();
                    }
                }, throwable -> {
                    // Then
                    assertEquals(true, false);
                    synchronized (syncObject) {
                        syncObject.notify();
                    }
                });

        synchronized (syncObject) {
            syncObject.wait(timeout);
        }
    }
}
