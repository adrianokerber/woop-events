package com.adrianokerber.woopevents;

import android.app.Application;

import com.adrianokerber.woopevents.framework.sdk.EventSDK;
import com.adrianokerber.woopevents.framework.util.ImageDownloader;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class WoopEventsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize SDK
        EventSDK.get().initialize();

        // Initialize RealmDB
        Realm.init(this);
        Realm.setDefaultConfiguration(
                new RealmConfiguration
                        .Builder()
                        .deleteRealmIfMigrationNeeded()
                        .build()
        );

        // Initialize image downloader
        ImageDownloader.init(this);
    }
}
