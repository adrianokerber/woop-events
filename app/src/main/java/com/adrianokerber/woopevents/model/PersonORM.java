package com.adrianokerber.woopevents.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class PersonORM extends RealmObject {
    public static String ID = "id";
    public static String NAME = "name";

    @PrimaryKey
    private int id;
    private EventORM event;
    private String name;
    private String picture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EventORM getEvent() {
        return event;
    }

    public void setEvent(EventORM event) {
        this.event = event;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
