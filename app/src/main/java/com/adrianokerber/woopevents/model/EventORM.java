package com.adrianokerber.woopevents.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class EventORM extends RealmObject {
    public static String ID = "id";
    public static String TITLE = "title";

    @PrimaryKey
    private String id;
    private String title;
    private String latitude;
    private String longitude;
    private String image;
    private String description;
    private Date date;
    private RealmList<PersonORM> people;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RealmList<PersonORM> getPeople() {
        return people;
    }

    public void setPeople(RealmList<PersonORM> people) {
        this.people = people;
    }
}
