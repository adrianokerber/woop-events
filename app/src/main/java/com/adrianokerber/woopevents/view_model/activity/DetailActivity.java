package com.adrianokerber.woopevents.view_model.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adrianokerber.woopevents.R;
import com.adrianokerber.woopevents.framework.sdk.event.EventAPI;
import com.adrianokerber.woopevents.framework.sdk.event.model.Checkin;
import com.adrianokerber.woopevents.framework.util.ImageDownloader;
import com.adrianokerber.woopevents.framework.util.RetryWithDelay;
import com.adrianokerber.woopevents.model.EventORM;
import com.adrianokerber.woopevents.model.PersonORM;
import com.adrianokerber.woopevents.view_model.adapter.PersonAdapter;

import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class DetailActivity extends AppCompatActivity {

    public static final String ID = "id";

    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        realm = Realm.getDefaultInstance();

        String eventId = getIntent().getStringExtra(ID);
        EventORM eventORM = realm.where(EventORM.class)
                .equalTo(EventORM.ID, eventId)
                .findFirst();

        // Name
        TextView titleTextView = findViewById(R.id.title);
        titleTextView.setText(eventORM.getTitle());
        // Thumbnail
        ImageView imageView = findViewById(R.id.image);
        String imageUrl = eventORM.getImage();
        ImageDownloader.load(imageUrl, imageView);
        // Description
        TextView description = findViewById(R.id.description);
        description.setText(eventORM.getDescription());

        // Checkin button
        FloatingActionButton checkin = findViewById(R.id.checkin);
        checkin.setOnClickListener(view -> {
            final Context context = this;
            new AlertDialog.Builder(this)
                    .setTitle(R.string.checkin)
                    .setView(R.layout.checkin_layout)
                    .setPositiveButton(R.string.checkin, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            EditText nameView = (EditText) ((AlertDialog) dialog).findViewById(R.id.name);
                            String name = nameView.getText().toString();
                            EditText emailView = (EditText) ((AlertDialog) dialog).findViewById(R.id.email);
                            String email = emailView.getText().toString();

                            EventAPI.get().postCheckin(new Checkin(eventId, name, email))
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .retryWhen(new RetryWithDelay(3, 2000))
                                    .subscribe(result -> {
                                        Toast.makeText(context, R.string.checkin_success, Toast.LENGTH_LONG)
                                                .show();
                                    }, throwable -> {
                                        Toast.makeText(context, R.string.checkin_failure, Toast.LENGTH_LONG)
                                                .show();
                                    });
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .create()
            .show();
        });

        // Load people list
        PersonAdapter adapter = new PersonAdapter(eventORM
                .getPeople()
                .where()
                .sort(PersonORM.NAME)
                .findAll());
        // Get the recycler view
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        // Attach the adapter to the RecyclerView to populate items
        recyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (realm != null) {
            realm.close();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
