package com.adrianokerber.woopevents.view_model.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adrianokerber.woopevents.R;
import com.adrianokerber.woopevents.framework.util.ImageDownloader;
import com.adrianokerber.woopevents.model.PersonORM;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder> {

    private RealmResults<PersonORM> personList;

    // To update the list data
    private Realm realm;
    private RealmChangeListener<RealmResults<PersonORM>> realmListener;

    // Holder that matches the model with the view
    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;

        public ViewHolder(View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.content);
        }
    }

    public PersonAdapter(RealmResults<PersonORM> queryResults) {
        realm = Realm.getDefaultInstance();

        personList = queryResults;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        realmListener = listener -> this.notifyDataSetChanged();
        personList.addChangeListener(realmListener);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

        // Stop watching list updates on Realm
        if(realm != null) {
            personList.removeChangeListener(realmListener);
            realm.close();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View couponView = inflater.inflate(R.layout.adapter_person, parent, false);

        // Return a new holder instance
        return new ViewHolder(couponView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        View personView = holder.view;
        PersonORM person = personList.get(position);

        if (person == null) {
            return;
        }

        // Set title
        TextView textView = personView.findViewById(R.id.name);
        textView.setText(person.getName());
        // Set picture
        ImageView imageView = personView.findViewById(R.id.picture);
        String imageUrl = person.getPicture();
        ImageDownloader.load(imageUrl, imageView);
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return personList.size();
    }
}
