package com.adrianokerber.woopevents.view_model.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.adrianokerber.woopevents.R;
import com.adrianokerber.woopevents.framework.util.RetryWithDelay;
import com.adrianokerber.woopevents.model.EventORM;
import com.adrianokerber.woopevents.model.PersonORM;
import com.adrianokerber.woopevents.framework.sdk.event.model.Event;
import com.adrianokerber.woopevents.framework.sdk.event.model.Person;
import com.adrianokerber.woopevents.framework.sdk.event.EventAPI;
import com.adrianokerber.woopevents.view_model.adapter.EventAdapter;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.exceptions.RealmException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private SwipeRefreshLayout reload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        updateData();

        EventAdapter adapter;
        try (Realm realm = Realm.getDefaultInstance()) {
            adapter = new EventAdapter(realm
                    .where(EventORM.class)
                    .sort(EventORM.TITLE)
                    .findAll());
        }
        // Get the recycler view
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        // Attach the adapter to the RecyclerView to populate items
        recyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Refresh logic for data from API
        reload = findViewById(R.id.reloadSwipeRefresh);
        reload.setOnRefreshListener(() -> {
            reload.setRefreshing(true);
            updateData();
        });
    }

    public void updateData() {
        EventAPI.get().getList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retryWhen(new RetryWithDelay(3, 2000))
                .subscribe(result -> {
                    try (Realm realm = Realm.getDefaultInstance()) {
                        realm.executeTransaction(realm1 -> {
                            // Clear old data
                            realm1.deleteAll();
                            // Load new data
                            for (Event event : result) {
                                // Event
                                EventORM eventORM = realm1.where(EventORM.class).
                                        equalTo(EventORM.ID, event.id).findFirst();
                                if (eventORM == null) {
                                    eventORM = realm1.createObject(EventORM.class, event.id);
                                }
                                eventORM.setTitle(event.title);
                                eventORM.setLatitude(event.latitude);
                                eventORM.setLongitude(event.longitude);
                                eventORM.setDescription(event.description);
                                eventORM.setImage(event.image);
                                // Date
                                Date date = new Date(event.date);
                                eventORM.setDate(date);
                                // People
                                RealmList<PersonORM> people = new RealmList<>();
                                for (Person person : event.people) {
                                    PersonORM personORM = realm1.createObject(PersonORM.class, person.id);
                                    personORM.setEvent(eventORM);
                                    personORM.setName(person.name);
                                    personORM.setPicture(person.picture);
                                    people.add(personORM);
                                }
                                eventORM.setPeople(people);
                            }
                        });
                    }

                    // Stop refreshing
                    reload.setRefreshing(false);
                }, throwable -> {
                    Log.e(TAG, throwable.toString());
                    // Stop refreshing
                    reload.setRefreshing(false);
                });
    }
}
