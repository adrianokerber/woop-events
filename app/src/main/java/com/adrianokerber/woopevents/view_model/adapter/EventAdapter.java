package com.adrianokerber.woopevents.view_model.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.adrianokerber.woopevents.R;
import com.adrianokerber.woopevents.framework.util.ImageDownloader;
import com.adrianokerber.woopevents.model.EventORM;
import com.adrianokerber.woopevents.view_model.activity.DetailActivity;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private RealmResults<EventORM> eventList;

    // To update the list data
    private Realm realm;
    private RealmChangeListener<RealmResults<EventORM>> realmListener;

    // Holder that matches the model with the view
    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;

        public ViewHolder(View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.content);
        }
    }

    public EventAdapter(RealmResults<EventORM> queryResults) {
        realm = Realm.getDefaultInstance();

        eventList = queryResults;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        realmListener = listener -> this.notifyDataSetChanged();
        eventList.addChangeListener(realmListener);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

        // Stop watching list updates on Realm
        if(realm != null) {
            eventList.removeChangeListener(realmListener);
            realm.close();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View couponView = inflater.inflate(R.layout.adapter_event, parent, false);

        // Return a new holder instance
        return new ViewHolder(couponView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        View eventView = holder.view;
        EventORM event = eventList.get(position);
        final Context context = eventView.getContext();

        if (event == null) {
            return;
        }

        // Set title
        TextView titleTextView = eventView.findViewById(R.id.title);
        titleTextView.setText(event.getTitle());
        // Set image
        ImageView imageView = eventView.findViewById(R.id.image);
        String imageUrl = event.getImage();
        ImageDownloader.load(imageUrl, imageView);

        // Set click behaviour
        eventView.setClickable(true);
        eventView.setFocusable(true);
        eventView.setOnClickListener(view -> {
            // Open details screen
            Intent intent = new Intent(context, DetailActivity.class);
            intent.putExtra(DetailActivity.ID, event.getId());
            context.startActivity(intent);
        });
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return eventList.size();
    }
}
