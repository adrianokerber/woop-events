package com.adrianokerber.woopevents.framework.sdk.event.model;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class Person {
    public String id;
    public String eventId;
    public String name;
    public String picture;
}
