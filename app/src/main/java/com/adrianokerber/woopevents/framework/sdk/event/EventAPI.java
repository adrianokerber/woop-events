package com.adrianokerber.woopevents.framework.sdk.event;

import com.adrianokerber.woopevents.framework.sdk.EventSDK;
import com.adrianokerber.woopevents.framework.sdk.event.model.Checkin;
import com.adrianokerber.woopevents.framework.sdk.event.model.Event;

import java.util.List;

import rx.Observable;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class EventAPI {
    private static final String TAG = EventAPI.class.getSimpleName();

    private static EventAPI instance;
    private EventService service;

    private EventAPI() {
        service = EventSDK.get().createServiceAsObservable(EventService.class);
    }

    public static EventAPI get() {
        if (instance == null) {
            instance = new EventAPI();
        }
        return instance;
    }

    /**
     * Get all events
     * @return
     */
    public Observable<List<Event>> getList() {
        return service.getList();
    }

    /**
     * Get a specific event
     * @param eventId the eventID
     * @return
     */
    public Observable<Event> getEvent(String eventId) {
        return service.getEvent(eventId);
    }

    /**
     * Register checkin of person
     * @param checkin
     * @return
     */
    public Observable<Void> postCheckin(Checkin checkin) {
        return service.postCheckin(checkin);
    }
}
