package com.adrianokerber.woopevents.framework.sdk.event.model;

import java.util.List;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class Event {
    public String id;
    public String title;
    public String latitude;
    public String longitude;
    public String image;
    public String description;
    public Long date;
    public List<Person> people;
}
