package com.adrianokerber.woopevents.framework.sdk;

import com.adrianokerber.woopevents.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class EventSDK {

    public static final String API_SCHEME = "http";
    public static final String API_HOST = "5b840ba5db24a100142dcd8c.mockapi.io";
    public static final Integer API_PORT = 80;
    public static final String API_PATH_SEGMENT = "api";

    private static EventSDK instance;

    private String token; // Auth token
    private HttpUrl serviceUrl;
    private String userAgent;

    private EventSDK() {}

    public static EventSDK get() {
        if (instance == null) {
            instance = new EventSDK();
        }
        return instance;
    }

    /**
     * Check if SDK is initialized
     * @return
     */
    public Boolean isInitialized() {
        return serviceUrl != null;
    }

    /**
     * TODO: this method is completely insecure since no authentication is used on API side. Please start using tokens!
     */
    public void initialize() {
        initialize("");
    }

    public void initialize(String token) {
        initialize(token, new HttpUrl.Builder()
                .scheme(API_SCHEME)
                .host(API_HOST)
                .port(API_PORT)
                .addPathSegment(API_PATH_SEGMENT)
                .addPathSegment("")
                .build());
    }

    public void initialize(String token, HttpUrl serviceUrl) {
        this.token = token;
        this.serviceUrl = serviceUrl;
        this.userAgent = "EventSDK/Android/version:" + BuildConfig.VERSION_NAME;
    }

    public <S> S createServiceAsObservable(Class<S> serviceClass) {
        if (!isInitialized()) throw new NotInitializedException();

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(40, TimeUnit.SECONDS);

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());

        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", getToken())
                        .header("User-Agent", getUserAgent())
                        .method(original.method(), original.body());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClientBuilder.build();
        Retrofit retrofit = retrofitBuilder.client(client).build();
        return retrofit.create(serviceClass);
    }

    private String getToken() {
        if (!isInitialized()) throw new NotInitializedException();
        return token;
    }

    public String getUserAgent() {
        if (!isInitialized()) throw new NotInitializedException();
        return userAgent;
    }
}
