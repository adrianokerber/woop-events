package com.adrianokerber.woopevents.framework.sdk.event.model;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class Checkin {

    private String eventId;
    private String name;
    private String email;

    public Checkin(String eventId, String name, String email) {
        this.eventId = eventId;
        this.name = name;
        this.email = email;
    }
}
