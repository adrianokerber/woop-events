package com.adrianokerber.woopevents.framework.sdk;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public class NotInitializedException extends RuntimeException {
    private static final String EXCEPTION_MESSAGE = "Before using EventSDK event you need to initialize it using EventSDK.get().initialize()";

    NotInitializedException() {
        super(EXCEPTION_MESSAGE);
    }
}
