package com.adrianokerber.woopevents.framework.sdk.event;

import com.adrianokerber.woopevents.framework.sdk.event.model.Checkin;
import com.adrianokerber.woopevents.framework.sdk.event.model.Event;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Adriano Kerber on 9/16/18.
 */
public interface EventService {

    @Headers("Content-Encoding: gzip, deflate")
    @GET("events")
    Observable<List<Event>> getList();

    @Headers("Content-Encoding: gzip, deflate")
    @GET("events/{event_id}")
    Observable<Event> getEvent(@Path("event_id") String eventId);

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("checkin")
    Observable<Void> postCheckin(@Body Checkin body);

}
