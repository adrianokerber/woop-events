# Change log

### Next

* Add animations
* Expand unit test beyond SDK

### 2018.09.16

Created project and addressed main functionalities

* Added events' list
* Added SDK for API calls control
* Added details screen
* Added people's list
* Added Realm DB
* Added refresh mechanism on MainActivity
* Added unit test for SDK